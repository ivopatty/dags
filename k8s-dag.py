from datetime import timedelta

from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.utils.dates import days_ago

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'schedule_interval': timedelta(days=1),
    'description': 'A simple calculator DAG based on K8s Pod Operators'
}

with DAG('Kubernetes', default_args=default_args) as dag:
    kubernetes_args = {
        'namespace': 'airflow',
        'image': 'zehbart/calculator:latest',
        'image_pull_policy': 'Always',
        'name': 'airflow-k8s-dag-pod'
    }
    subtract = KubernetesPodOperator(
        task_id='subtract',
        arguments=["--method", 'subtract', '--values', '5', '2'],
        default_args=kubernetes_args)
    multiply = KubernetesPodOperator(
        task_id='multiply',
        arguments=["--method", 'subtract', '--values', '8', '5'],
        default_args=kubernetes_args)
    divide = KubernetesPodOperator(
        task_id='divide',
        arguments=["--method", 'divide', '--values', '1600', '4'],
        default_args=kubernetes_args)
    add = KubernetesPodOperator(
        task_id='add',
        arguments=["--method", 'add', '--values', '134', '9'],
        default_args=kubernetes_args)

    dag >> subtract >> [multiply, divide] >> add
