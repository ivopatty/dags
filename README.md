# Setting up Airflow on K8s

To imitate a cloud-native Airflow setup, we're using a
helm chart to deploy airflow to K8s and sync in our DAGs
from a git repository

## Enable Kubernetes

If you haven't already, open your Docker Desktop settings
and enable Kubernetes from your local settings menu.

Apply and restart your Docker engine for this to take effect.

## Install Helm
```shell script
brew install helm
# or
choco install kubernetes-helm
```

Or download yourself a [binary](https://github.com/helm/helm/releases/tag/v2.16.10)

Next add the helm repo we'll be using today:
```shell script
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update
```

## Deploy Airflow

```shell script
kubectl create ns airflow

helm install airflow stable/airflow \
  --version "7.5.0" \
  --namespace "airflow" \
  --values ./airflow-values.yaml \
  --wait
```

And access your web interface via:

```shell script
export POD_NAME=$(kubectl get pods --namespace airflow -l "component=web,app=airflow" -o jsonpath="{.items[0].metadata.name}")
echo http://127.0.0.1:8080
kubectl port-forward --namespace airflow $POD_NAME 8080:8080
```