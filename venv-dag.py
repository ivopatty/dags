from datetime import timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from operator import __mul__, __sub__, __add__, __truediv__

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    'schedule_interval': timedelta(days=1),
    'description': 'A simple calculator DAG based on Python Venvs'

}


def do_math(math_operation, math_values):
    """
    We do some sick math here

    :param math_operation:  How to math
    :param math_values:     What to math
    :return:                What was math'ed
    """
    from functools import reduce
    return reduce(math_operation, math_values)


with DAG('VenvDag', default_args=default_args) as dag:

    venv_parameters = {
        'dag': dag,
        'requirements': ['numpy', 'pandas'],  # We don't actually need these, but simulate a small DS scenario
        'python_callable': do_math
    }

    subtract = PythonVirtualenvOperator(
        default_args=venv_parameters,
        op_args=[__sub__, [5, 2]],
        task_id='subtract'
    )
    multiply = PythonVirtualenvOperator(
        default_args=venv_parameters,
        op_args=[__mul__, [8, 5]],
        task_id='multiply'
    )
    divide = PythonVirtualenvOperator(
        default_args=venv_parameters,
        op_args=[__truediv__, [1600, 4]],
        task_id='divide'
    )
    add = PythonVirtualenvOperator(
        default_args=venv_parameters,
        op_args=[__add__, [134, 9]],
        task_id='add'
    )

    dag >> subtract >> [multiply, divide] >> add
